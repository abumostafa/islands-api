# Islands RPG API

This is the presentation layer for the Islands RPG

this API is built on top of Silex Framework but you can implement with any framework

Islands RPG has it's own handlers to easy inject into your controllers and everything will be handled.

below you can find request/response examples

## Installation

- install composer packages `` composer install``
- set your environment parameters ``mv app/config/parameters.php.dist app/config/parameters.php``
- edit ``parameters.php`` with your configuration


#### Create user
Request

```bash
POST /v1/users HTTP/1.1
Content-Type: application/json
Cache-Control: no-cache

{"username": "some-unique-username"}
```

Response
```
{
  "data": {
    "id": "1",
    "username": "some-unique-username"
  }
}
```

#### Create new game
Request

```
POST /v1/users/1/game HTTP/1.1
Content-Type: application/json
Cache-Control: no-cache

{"map_id": "1", "character_id": "1"}
```

Response
```
{
   "data": {
     "id": "2",
     "position": "9",
     "map": {
       "id": "1",
       "visible_squares": [
         9,
         10,
         14,
         15,
         16,
         17,
         20,
         21,
         22,
         23,
         27,
         28
       ],
       "start_position": "9"
     },
     "player": {
       "id": "2",
       "user_id": "4",
       "character": {
         "id": "1",
         "name": "John",
         "image": "https://image.ibb.co/gBKmVa/male.png"
       }
     }
   }
}
```

#### Fetch existing game
Request

```
GET /v1/users/1/game/1 HTTP/1.1
Content-Type: application/json
Cache-Control: no-cache
```

Response
```
{
  "data": {
    "id": "1",
    "position": "9",
    "map": {
      "id": "1",
      "visible_squares": [
        9,
        10,
        14,
        15,
        16,
        17,
        20,
        21,
        22,
        23,
        27,
        28
      ],
      "start_position": "9"
    },
    "player": {
      "id": "2",
      "user_id": "4",
      "character": {
        "id": "1",
        "name": "John",
        "image": "https://image.ibb.co/gBKmVa/male.png"
      }
    }
  }
}
```

#### Explode the map
Request
```
PATCH /v1/users/1/game/1 HTTP/1.1
Content-Type: application/json
Cache-Control: no-cache

{"position": "14"}
```

Response ``204 No Content``

#### Attack an enemy
Request
```
POST /v1/users/1/game/1/attack/1 HTTP/1.1
Content-Type: application/json
Cache-Control: no-cache

{"enemy_id": "2"}
```

Response ``201 Created``

#### Fetch player characters
Request
```
GET /v1/player_characters HTTP/1.1
Content-Type: application/json
Cache-Control: no-cache
```

Response
```
{
  "data": [
    {
      "id": "1",
      "name": "John",
      "image": "https://image.ibb.co/gBKmVa/male.png"
    },
    {
      "id": "2",
      "name": "Sussan",
      "image": "https://image.ibb.co/myah3v/female.png"
    }
  ]
}
```

#### Fetch maps
Request
```
GET /v1/maps HTTP/1.1
Content-Type: application/json
Cache-Control: no-cache

```

Response
```
{
  "data": [
    {
      "id": "1",
      "name": "Usedom",
      "visible_squares": "[9, 10, 14, 15, 16, 17, 20, 21, 22, 23, 27, 28]",
      "start_position": "9"
    },
    {
      "id": "2",
      "name": "Fehmarn",
      "visible_squares": "[8, 9, 10, 11, 14, 15, 16, 17, 20, 21, 22, 23, 26, 27, 28, 29]",
      "start_position": "8"
    }
  ]
}
```