<?php

/**
 * @author         Ahmed Abumostafa <iam.abumostafa@gmail.com>
 * @date           08/04/2017
 * @project        IslandsApi
 */

require_once __DIR__ . '/../vendor/autoload.php';

$app = new Silex\Application(['debug' => true]);

require_once __DIR__ . '/../app/bootstrap/bootstrap.php';
require_once __DIR__ . '/../app/bootstrap/services.php';
require_once __DIR__ . '/../app/config/routes.php';

$app->run();