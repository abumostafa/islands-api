<?php

/**
 * @author         Ahmed Abumostafa <iam.abumostafa@gmail.com>
 * @date           09/04/2017
 * @project        IslandsApi
 * @package        IslandsApi\Support\Validation
 */

namespace IslandsApi\Support\Validation;

use Symfony\Component\Validator\ConstraintViolationListInterface;

class Validation
{
    public static function beautifyErrors(ConstraintViolationListInterface $errors, $firstErrorOnly = true)
    {
        $result = [];

        foreach ($errors as $error) {

            if ($firstErrorOnly && isset($result[$error->getPropertyPath()])) {
                continue;
            }

            if ($firstErrorOnly) {
                $result[$error->getPropertyPath()] = $error->getMessage();
            } else {
                $result[$error->getPropertyPath()][] = $error->getMessage();
            }
        }

        return $result;
    }
}