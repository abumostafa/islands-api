<?php

/**
 * @author         Ahmed Abumostafa <iam.abumostafa@gmail.com>
 * @date           09/04/2017
 * @project        IslandsApi
 * @package        IslandsApi\Support\Constraint
 */

namespace IslandsApi\Support\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Database Exists Constraint
 *
 * @package IslandsApi\Support\Constraint
 */
class DatabaseExists extends Constraint
{
    /**
     * @var string
     */
    public $table;

    /**
     * @var string
     */
    public $column = 'id';

    /**
     * @var string
     */
    public $message = 'value not found';

    /**
     * @inheritDoc
     */
    public function validatedBy()
    {
        return 'validator.exists';
    }
}