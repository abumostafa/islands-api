<?php

/**
 * @author         Ahmed Abumostafa <iam.abumostafa@gmail.com>
 * @date           09/04/2017
 * @project        IslandsApi
 * @package        IslandsApi\Support\Constraint
 */

namespace IslandsApi\Support\Constraint;

use Doctrine\DBAL\Connection;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 *
 *
 * @package IslandsApi\Support\Constraint
 */
class DatabaseExistsValidator extends ConstraintValidator
{
    /**
     * @var Connection
     */
    protected $con;

    /**
     * @param Connection $con
     */
    public function setConnection(Connection $con)
    {
        $this->con = $con;
    }

    /**
     * @inheritDoc
     */
    public function validate($value, Constraint $constraint)
    {
        if (!$value) {
            return $value;
        }

        if (!$constraint instanceof DatabaseExists) {
            throw new \InvalidArgumentException('Constraint must be instance of ' . DatabaseExists::class);
        }

        $count = $this->con->fetchColumn(
            sprintf('SELECT count(%s) as cnt from %s where %s = ?', $constraint->column, $constraint->table, $constraint->column),
            [$value]
        );

        if ($count === "0") {
            $this->context->buildViolation($constraint->message)->addViolation();
        }
    }
}