<?php

/**
 * @author         Ahmed Abumostafa <iam.abumostafa@gmail.com>
 * @date           09/04/2017
 * @project        IslandsApi
 * @package        IslandsApi\Http\Controller
 */

namespace IslandsApi\Http\Controller;

use Silex\Application;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Abstract Controller
 *
 * @package IslandsApi\Http\Controller
 */
abstract class AbstractController
{
    /**
     * @var Application
     */
    private $container;

    /**
     * AbstractController constructor.
     * @param Application $container
     */
    public function __construct(Application $container)
    {
        $this->container = $container;
    }

    /**
     * @param $id
     * @return mixed
     */
    protected function get($id)
    {
        return $this->container[$id];
    }

    /**
     * @return Request
     */
    protected function getRequest()
    {
        return $this->get('request_stack')->getCurrentRequest();
    }

    /**
     * @param array $errors
     * @param string $ns
     * @return JsonResponse
     */
    protected function badRequest($errors, $ns = 'error')
    {
        return new JsonResponse([$ns => $errors], 400);
    }

    /**
     * @param array $data
     * @param string $ns
     * @return JsonResponse
     */
    protected function json($data, $ns = 'data')
    {
        return new JsonResponse([$ns => $data]);
    }
}