<?php

/**
 * @author         Ahmed Abumostafa <iam.abumostafa@gmail.com>
 * @date           09/04/2017
 * @project        IslandsApi
 * @package        IslandsApi\Http\Controller
 */

namespace IslandsApi\Http\Controller;

use Islands\Handler\MapHandler;

/**
 * Map Controller
 *
 * @package IslandsApi\Http\Controller
 */
class MapController extends AbstractController
{
    public function listAction()
    {
        return $this->json(
            $this->getHandler()->handleList()
        );
    }

    /**
     * @return MapHandler
     */
    protected function getHandler()
    {
        return $this->get('handler.map');
    }
}