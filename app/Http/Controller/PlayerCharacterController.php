<?php

/**
 * @author         Ahmed Abumostafa <iam.abumostafa@gmail.com>
 * @date           09/04/2017
 * @project        IslandsApi
 * @package        IslandsApi\Http\Controller
 */

namespace IslandsApi\Http\Controller;

use Islands\Handler\PlayerCharacterHandler;

/**
 * Player Character Controller
 *
 * @package IslandsApi\Http\Controller
 */
class PlayerCharacterController extends AbstractController
{
    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function listAction()
    {
        return $this->json(
            $this->getHandler()->handleList()
        );
    }

    /**
     * @return PlayerCharacterHandler
     */
    protected function getHandler()
    {
        return $this->get('handler.player.character');
    }
}