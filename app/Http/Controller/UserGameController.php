<?php

/**
 * @author         Ahmed Abumostafa <iam.abumostafa@gmail.com>
 * @date           09/04/2017
 * @project        IslandsApi
 * @package        IslandsApi\Http\Controller
 */

namespace IslandsApi\Http\Controller;

use Islands\Handler\UserGameHandler;
use Islands\Exception\DatabaseObjectNotFoundException;
use Islands\Model\Game;
use Islands\Model\User;
use IslandsApi\Exception\InvalidFormException;
use IslandsApi\Form\CreateUserGameType;
use IslandsApi\Form\UpdateUserGameType;
use IslandsApi\Support\Validation\Validation;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Validator\Constraints\Callback;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Context\ExecutionContext;
use Symfony\Component\Validator\Validator\ContextualValidatorInterface;

/**
 * User Game Controller
 *
 * @package IslandsApi\Http\Controller
 */
class UserGameController extends AbstractController
{
    /**
     * Create new Game
     *
     * @param $userId
     * @return JsonResponse
     */
    public function createAction($userId)
    {
        $data = $this->getRequest()->request->all();

        try {

            $form = $this->get('form.factory')->create(CreateUserGameType::class, [])->submit($data);

            $this->validateCreateForm($form);

            $game = $this->getHandler()->handleCreate($userId, $data);

            return $this->renderGame($game);
        } catch (InvalidFormException $ex) {
            return $this->badRequest($ex->getErrors());
        } catch (DatabaseObjectNotFoundException $ex) {
            throw new NotFoundHttpException();
        }
    }

    /**
     * Partial update an existing game
     *
     * @param $userId
     * @param $gameId
     * @return Response|JsonResponse
     */
    public function patchAction($userId, $gameId)
    {
        try {
            /** @var User $user */
            $user = $this->get('manager.user')->findOneOrFail(['id' => $userId]);

            /** @var Game $game */
            $game = $this->get('manager.user.game')->findOneOrFail(['id' => $gameId]);

            if ($game->getPlayer()->getUser()->getId() !== $user->getId()) {
                throw new NotFoundHttpException();
            }

            $data = $this->getRequest()->request->all();

            $form = $this->get('form.factory')->create(UpdateUserGameType::class, [])->submit($data);

            $errors = $this->get('validator')->validate($form->getData(), new Collection([
                'position' => [
                    new NotBlank(),
                    new Callback(['callback' => function ($value, ExecutionContext $context) use ($game) {

                        if (false === in_array($value, $game->getMap()->getVisibleSquares())) {
                            $context->addViolation('Square not found');
                        }
                    }]),
                ]
            ]));

            if (count($errors)) {
                throw new InvalidFormException(
                    Validation::beautifyErrors($errors)
                );
            }

            $game = $this->getHandler()->handleUpdate($gameId, $form->getData());

            return new Response('', Response::HTTP_NO_CONTENT);
        } catch (InvalidFormException $ex) {
            return $this->badRequest($ex->getErrors());
        } catch (DatabaseObjectNotFoundException $ex) {
            throw new NotFoundHttpException();
        }
    }

    /**
     * Fetch an existing game
     *
     * @param $userId
     * @param $gameId
     *
     * @return JsonResponse
     */
    public function fetchAction($userId, $gameId)
    {
        try {
            /** @var User $user */
            $user = $this->get('manager.user')->findOneOrFail(['id' => $userId]);

            /** @var Game $game */
            $game = $this->get('manager.user.game')->findOneOrFail(['id' => $gameId]);

            if ($game->getPlayer()->getUser()->getId() !== $user->getId()) {
                throw new NotFoundHttpException();
            }

            $game = $this->getHandler()->handleFetch($gameId);

            return $this->renderGame($game);

        } catch (DatabaseObjectNotFoundException $ex) {
            throw new NotFoundHttpException();
        }
    }

    /**
     * @return UserGameHandler
     */
    protected function getHandler()
    {
        return $this->get('handler.user.game');
    }

    /**
     * @param FormInterface $form
     *
     * @throws InvalidFormException
     */
    public function validateCreateForm(FormInterface $form)
    {
        $constraints = new Collection([
            'map_id' => [new NotBlank(), new Callback(['callback' => [$this, 'validateMap']])],
            'character_id' => [new NotBlank(), new Callback(['callback' => [$this, 'validateCharacter']])],
        ]);

        $errors = $this->get('validator')->validate($form->getData(), $constraints);

        if (count($errors)) {
            throw new InvalidFormException(
                Validation::beautifyErrors($errors)
            );
        }
    }

    /**
     * @param $value
     * @param ExecutionContext $context
     */
    public function validateMap($value, ExecutionContext $context)
    {
        try {
            $this->get('manager.map')->findOneOrFail(['id' => $value]);
        } catch (DatabaseObjectNotFoundException $e) {

            $context->addViolation('Map not found');
        }
    }

    /**
     * @param $value
     * @param ExecutionContext $context
     */
    public function validateCharacter($value, ExecutionContext $context)
    {
        try {
            $row = $this->get('manager.character')->findOneOrFail(['id' => $value]);
        } catch (DatabaseObjectNotFoundException $e) {
            $context->addViolation('Character not found');
        }
    }

    /**
     * @param Game $game
     * @return JsonResponse
     */
    public function renderGame(Game $game)
    {
        return $this->json([
            'id' => $game->getId(),
            'position' => $game->getPosition(),
            'map' => [
                'id' => $game->getMap()->getId(),
                'visible_squares' => $game->getMap()->getVisibleSquares(),
                'start_position' => $game->getMap()->getStartPosition(),
            ],
            'player' => [
                'id' => $game->getPlayer()->getId(),
                'user_id' => $game->getPlayer()->getUser()->getId(),
                'character' => [
                    'id' => $game->getPlayer()->getCharacter()->getId(),
                    'name' => $game->getPlayer()->getCharacter()->getName(),
                    'image' => $game->getPlayer()->getCharacter()->getImage(),
                ]
            ]
        ]);
    }
}