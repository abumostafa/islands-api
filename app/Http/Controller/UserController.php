<?php

/**
 * @author         Ahmed Abumostafa <iam.abumostafa@gmail.com>
 * @date           09/04/2017
 * @project        IslandsApi
 * @package        IslandsApi\Http\Controller
 */

namespace IslandsApi\Http\Controller;

use Islands\Model\User;
use IslandsApi\Exception\InvalidFormException;
use Islands\Handler\UserHandler;
use IslandsApi\Form\CreateUserType;
use IslandsApi\Support\Validation\Validation;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

/**
 * User Controller
 *
 * @package IslandsApi\Http\Controller
 */
class UserController extends AbstractController
{
    /**
     * @return JsonResponse
     */
    public function createAction()
    {
        try {
            $data = $this->getRequest()->request->all();

            /** @var FormInterface $form */
            $form = $this->get('form.factory')->create(CreateUserType::class, new User())->submit($data);

            $errors = $this->get('validator')->validate(['username' => $form->getData()->getUsername()], new Collection([
                'username' => [
                    new NotBlank(),
                    new Length(array('min' => 3)),
                ]
            ]));

            if (count($errors)) {
                throw new InvalidFormException(
                    Validation::beautifyErrors($errors)
                );
            }

            $user = $this->getHandler()->handleCreate($form->getData());

            return $this->json([
                'id' => $user->getId(),
                'username' => $user->getUsername(),
            ]);
        } catch (InvalidFormException $ex) {
            return $this->badRequest($ex->getErrors());
        }
    }

    /**
     * @return UserHandler
     */
    protected function getHandler()
    {
        return $this->get('handler.user');
    }
}