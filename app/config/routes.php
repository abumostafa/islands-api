<?php

use Symfony\Component\HttpFoundation\Request;

$app->before(function (Request $request) {
    if (0 === strpos($request->headers->get('Content-Type'), 'application/json')) {
        $data = json_decode($request->getContent(), true);
        $request->request->replace(is_array($data) ? $data : []);
    }
});

/* user routes */
$app->post('/v1/users', 'controller.user:createAction');

/* user game routes */
$app->post('/v1/users/{userId}/game', function ($userId) use ($app) {
    return $app['controller.user.game']->createAction($userId);
});

$app->get('/v1/users/{userId}/game/{gameId}', function ($userId, $gameId) use ($app) {
    return $app['controller.user.game']->fetchAction($userId, $gameId);
});

$app->patch('/v1/users/{userId}/game/{gameId}', function ($userId, $gameId) use ($app) {
    return $app['controller.user.game']->patchAction($userId, $gameId);
});

$app->patch('/v1/users/{userId}/game/{gameId}/attack/{enemyId}', function ($userId, $gameId, $enemyId) use ($app) {
    return $app['controller.user.game.attack']->patchAction($userId, $gameId, $enemyId);
});

/* player character routes */
$app->get('/v1/player_characters', 'controller.player.character:listAction');

/* map routes */
$app->get('/v1/maps', 'controller.map:listAction');