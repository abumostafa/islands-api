<?php

/**
 * @author         Ahmed Abumostafa <iam.abumostafa@gmail.com>
 * @date           09/04/2017
 * @project        IslandsApi
 * @package        IslandsApi\Provider
 */

namespace IslandsApi\Provider;

use IslandsApi\Support\Constraint\DatabaseExistsValidator;
use Pimple\Container;
use Pimple\ServiceProviderInterface;

/**
 * Database Exists Validator Service Provider
 *
 * @package IslandsApi\Provider
 */
class DatabaseExistsValidatorServiceProvider implements ServiceProviderInterface
{
    public function register(Container $app)
    {
        $app['database.exists.validator'] = function () use ($app) {
            $validator = new DatabaseExistsValidator();
            $validator->setConnection($app['db']);
            return $validator;
        };
    }
}