<?php

$parameters = require __DIR__ . '/../config/parameters.php';

$app->register(new Silex\Provider\ServiceControllerServiceProvider());
$app->register(new Silex\Provider\RoutingServiceProvider());
$app->register(new IslandsApi\Provider\DatabaseExistsValidatorServiceProvider());
$app->register(new Silex\Provider\FormServiceProvider());
$app->register(new Silex\Provider\DoctrineServiceProvider(), [
        'db.options' => [
            'driver' => 'pdo_mysql',
            'host' => $parameters['database']['host'],
            'dbname' => $parameters['database']['dbname'],
            'user' => $parameters['database']['username'],
            'password' => $parameters['database']['password'],
            'charset' => 'utf8',
        ],
    ]
);
$app->register(new Silex\Provider\ValidatorServiceProvider(), [
    'validator.validator_service_ids' => array(
        'validator.exists' => 'database.exists.validator',
    )
]);
