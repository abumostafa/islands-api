<?php

# Services

/* Managers */
$app['manager.user'] = function ($app) {
    return new Islands\Manager\PDO\UserManager($app['db']);
};

$app['manager.character'] = function ($app) {
    return new Islands\Manager\PDO\CharacterManager($app['db']);
};

$app['manager.map'] = function ($app) {
    return new Islands\Manager\PDO\MapManager($app['db']);
};

$app['manager.game.player'] = function ($app) {
    return new Islands\Manager\PDO\GamePlayerManager($app['db'], $app['manager.user'], $app['manager.character']);
};

$app['manager.user.game'] = function ($app) {
    return new Islands\Manager\PDO\UserGameManager($app['db'], $app['manager.game.player'], $app['manager.map']);
};

/* Service */
$app['service.game'] = function ($app) {
    return new \Islands\Service\GameService($app['manager.user.game']);
};

/* Handlers */
$app['handler.user'] = function ($app) {
    return new Islands\Handler\UserHandler($app['manager.user']);
};

$app['handler.user.game'] = function ($app) {
    return new Islands\Handler\UserGameHandler(
        $app['manager.user'],
        $app['service.game']
    );
};

$app['handler.player.character'] = function ($app) {
    return new Islands\Handler\PlayerCharacterHandler($app['manager.character']);
};

$app['handler.map'] = function ($app) {
    return new Islands\Handler\MapHandler($app['manager.map']);
};

/* Controllers */
$app['controller.user'] = function ($app) {
    return new IslandsApi\Http\Controller\UserController($app);
};

$app['controller.user.game'] = function ($app) {
    return new IslandsApi\Http\Controller\UserGameController($app);
};

$app['controller.player.character'] = function ($app) {
    return new IslandsApi\Http\Controller\PlayerCharacterController($app);
};

$app['controller.map'] = function ($app) {
    return new IslandsApi\Http\Controller\MapController($app);
};
